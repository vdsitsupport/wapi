<?php

namespace AthenaTT\Wapi;

use Illuminate\Support\Facades\Http;

class Wapi
{
    // list of all urls that are available. 
    private $routes = [
        'user' => '/user', // Route to get user information, such as ID. 
        'userdata' => '/user/%id%', // get user information.
        'userworlds' => '/user/%id%/worlds', // user worlds. use User id. 
        'world' => '/world/%id%', // get world data. 
        'worldarticles' => '/world/%id%/articles', // get world articles. Use World ID
        'article' => '/article/%id%', // article information. 
        'worldblocks' => '/world/%id%/blocks', // get world blocks. Use world ID
        'block' => '/block/%id%', // block information. 
    ];
    public $app = null; // App key
    public $user = null; // user token. 
    public $baseUrl = null; // Aragorn base URL. 

    public static function initialize($app, $user, $baseUrl = 'https://www.worldanvil.com/api/aragorn')
    {
        $wapi = new Wapi;
        $wapi->app = $app;
        $wapi->user = $user;
        $wapi->baseUrl = $baseUrl;
        return $wapi;
    }

    public function index()
    {
        // return some view here that shows stuff. 

    }

    public function user()
    {
        $url = $this->baseUrl . $this->routes['user'];
        return $this->retrieve($url);
    }

    public function userdata($id)
    {
        $url = $this->baseUrl . strtr($this->routes['userdata'], array('%id%' => $id));
        return $this->retrieve($url);
    }

    public function userworlds($id)
    {
        $url = $this->baseUrl . strtr($this->routes['userworlds'], array('%id%' => $id));
        return $this->retrieve($url);
    }

    public function world($id)
    {
        $url = $this->baseUrl . strtr($this->routes['world'], array('%id%' => $id));
        return $this->retrieve($url);
    }
    public function worldarticles($id, $offset = 0, $term = null, $orderBy = 'id', $trajectory = 'asc')
    {
        $url = $this->baseUrl . strtr($this->routes['worldarticles'], array('%id%' => $id)) . "?offset=$offset";

        if ($term != null) { // search term to search for in articles. Only if its set. 
            $url = $url . '&term=' . urlencode(strtolower($term));
        }
        $url = $url . '&order_by=' . urlencode(strtolower($orderBy)); // add orderby
        $url = $url . '&trajectory=' . urlencode(strtolower($trajectory)); // add trajectory

        return $this->retrieve($url);
    }
    public function article($id)
    {
        $url = $this->baseUrl . strtr($this->routes['article'], array('%id%' => $id));
        return $this->retrieve($url);
    }
    public function worldblocks($id, $offset = 0, $orderBy = 'id', $trajectory = 'asc')
    {
        $url = $this->baseUrl . strtr($this->routes['worldblocks'], array('%id%' => $id));
        $url = $url . '?order_by=' . urlencode(strtolower($orderBy)); // add orderby
        $url = $url . '&trajectory=' . urlencode(strtolower($trajectory)); // add trajectory
        return $this->retrieve($url);
    }
    public function block($id)
    {
        $url = $this->baseUrl . strtr($this->routes['block'], array('%id%' => $id));
        return $this->retrieve($url);
    }

    private function retrieve($url)
    {
        $response = Http::withHeaders([
            'Content-type' => ' application/json',
            'x-auth-token' => $this->user,
            'x-application-key' => $this->app,
            'User-Agent' => 'WAPI (V0.1)'
        ])->get($url);
        \Debugbar::info('url', $url);
        \Debugbar::info('response', $response);
        return $response;
    }
}
